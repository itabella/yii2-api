<?php
/**
 * Created by PhpStorm.
 * User: itabella
 * Date: 23/09/2020
 * Time: 17.00
 */

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=qram',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
];