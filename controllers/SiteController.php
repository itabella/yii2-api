<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

class SiteController extends Controller
{
    public function actionIndex()
    {
        // update comment
        $post = Yii::$app->db->createCommand('select * from accounts where id = 1')->queryOne();
        return json_encode($post);
    }
}