<?php

namespace helpers;

use yii\db\Exception;

class BaseHelper
{
    public static function getIpAddress()
    {
        $tmp = array();
        $servers = [
            // 'HTTP_CLIENT_IP',
            'HTTP_X_REAL_IP',
            // 'HTTP_X_FORWARDED_FOR',
            'REMOTE_ADDR'
        ];
        foreach ($servers as $key) {
            if (array_key_exists($key, $_SERVER)) {
                if (filter_var($_SERVER[$key], FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE))
                    return $_SERVER[$key];
                $tmp[] = $_SERVER[$key];
            }
        }
        return reset($tmp);
    }

    public static function getDataFromKey($keywords, $data)
    {
        try {
            return array_filter($data, function ($value, $key) use ($keywords) {
                if (strpos($key, $keywords) !== false) {
                    return $value;
                }
            }, ARRAY_FILTER_USE_BOTH);
        } catch (\yii\db\Exception $e) {
            return false;
        }
    }

    public static function getValue($arr, $key, $def = null)
    {
        return isset($arr[$key]) ? $arr[$key] : $def;
    }

    public static function toArray($data)
    {
        return json_decode($data, true);
    }

    public static function issetKey($key, $data, $default = null)
    {
        if (is_array($data)) {
            return array_key_exists($key, $data) ? $array[$key] : $default;
        }

        if (is_object($data)) {
            return property_exists($data, $key) ? $data->$key : $default;
        }

        return false;
    }

    public static function writeFile($data_replace, $file)
    {
        try {
            $fp = fopen($file, 'wb');
            fwrite($fp, $data_replace);
            fclose($fp);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public static function maskPhoneNumber($phone, $showFirstDigits = 3, $showLastDigits = 3)
    {
        // showFirstDigits how many digits to show in the beggining of the phone number
        // showLastDigits how many digits to show in the end of the phone number
        $mask_number = (substr_replace(
            $phone,
            str_repeat('x', strlen($phone) - $showFirstDigits - $showLastDigits),
            $showFirstDigits,
            strlen($phone) - $showFirstDigits - $showLastDigits
        ));

        $number = str_split($mask_number);
        $mask_number = null;
        for ($i = 0; $i < count($number); $i++) {
            if ($i % 4 == 0 && $i != 0) {
                $mask_number .= '-';
            }
            $mask_number .= $number[$i];
        }

        return $mask_number;
    }

    public static function currency($data)
    {
        return "Rp. " . number_format($data, 2, ',', '.');
    }

    public static function convert_to_number($currency)
    {
        return intval(preg_replace('/,.*|[^0-9]/', '', $currency));
    }

    public static function generateRandom($length, $low = 'A', $high = 'Z')
    {
        //number : low = 0; high = 9
        //upper : low = A; high = Z
        //lower : low = a; high = z
        return substr(str_shuffle(implode(range($low, $high))), 1, $length);
    }

    public static function generateSymbol($length)
    {
        return substr(str_shuffle('~!@#$%&?.,=+-*'), 1, $length);
    }
}